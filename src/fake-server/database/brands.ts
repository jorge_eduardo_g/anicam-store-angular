import { BrandDef } from '../interfaces/brand-def';
import { Brand } from '../../app/shared/interfaces/brand';
import { Observable, of } from 'rxjs';

let lastBrandId = 0;

const brandsDef: BrandDef[] = [
    {name: 'Pyle', slug: 'pyle', image: 'assets/images/logos/brands/pyle.png'},
    {name: 'Nutrichef', slug: 'nutrichef', image: 'assets/images/logos/brands/nutrichef.png'},
    {name: 'SereneLife', slug: 'serenelife', image: 'assets/images/logos/brands/serenelife.png'},
    {name: 'Sandisk', slug: 'sandisk', image: 'assets/images/logos/brands/sandisk.png'},
    {name: 'Wps', slug: 'wps-inc', image: 'assets/images/logos/brands/wps.png'},
    {name: 'Arai', slug: 'arai', image: 'assets/images/logos/brands/arai.png'},
    {name: 'PureClean', slug: 'pureclean', image: 'assets/images/logos/brands/pureclean.png'},
    {name: 'Metaggo', slug: 'metaggo', image: 'assets/images/logos/logo-7.png'},
];

export const brands: Brand[] = brandsDef.map(brandDef => {
    return {
        ...brandDef,
        id: ++lastBrandId,
    };
});

export function getBrands(): Observable<Brand[]> {
    return of(brands);
}
