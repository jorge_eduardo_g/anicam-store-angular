import { Component, Input } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { DirectionService } from '../../../shared/services/direction.service';
import {TranslateService} from "@ngx-translate/core";

@Component({
    selector: 'app-block-slideshow',
    templateUrl: './block-slideshow.component.html',
    styleUrls: ['./block-slideshow.component.scss']
})
export class BlockSlideshowComponent {
    @Input() withDepartments = false;

    options = {
        nav: false,
        dots: true,
        loop: true,
        responsive: {
            0: {items: 1}
        },
        rtl: this.direction.isRTL()
    };

    slides: object[]

    constructor(
        public sanitizer: DomSanitizer,
        private direction: DirectionService,
        private readonly translateService: TranslateService
    ) { }

    ngOnInit(): void {

        this.translateService.get(['']).subscribe(translations => {
             // console.info(this.translateService.instant('key1'));
            this.slides = [
                {
                    title: this.translateService.instant('primaryBanner.first.title'),
                    text: this.translateService.instant('primaryBanner.first.text'),
                    image_classic: 'assets/images/slides/store/1.png',
                    image_full: 'assets/images/slides/store/1.png',
                    image_mobile: 'assets/images/slides/slide-1-mobile.jpg',
                    button: this.translateService.instant('primaryBanner.first.button'),
                    url: this.translateService.instant('primaryBanner.first.url'),
                },
                {
                    title: `<span style="color: #fff"> ${this.translateService.instant('primaryBanner.second.title')} </span>`,
                    text: `<span style="color: #fff"> ${this.translateService.instant('primaryBanner.second.text')} </span>`,
                    image_classic: 'assets/images/slides/store/3.png',
                    image_full: 'assets/images/slides/slide-2-full.jpg',
                    image_mobile: 'assets/images/slides/slide-2-mobile.jpg',
                    button: this.translateService.instant('primaryBanner.second.button'),
                    url: this.translateService.instant('primaryBanner.second.url'),
                },
                {
                    title: this.translateService.instant('primaryBanner.third.title'),
                    text: this.translateService.instant('primaryBanner.third.text'),
                    image_classic: 'assets/images/slides/store/5.png',
                    image_full: 'assets/images/slides/slide-3-full.jpg',
                    image_mobile: 'assets/images/slides/slide-3-mobile.jpg',
                    button: this.translateService.instant('primaryBanner.third.button'),
                    url: this.translateService.instant('primaryBanner.third.url'),
                },
                {
                    title: `<span style="color: #fff"> ${this.translateService.instant('primaryBanner.fourth.title')} </span>`,
                    text: `<span style="color: #fff"> ${this.translateService.instant('primaryBanner.fourth.text')} </span>`,
                    image_classic: 'assets/images/slides/store/7.png',
                    image_full: 'assets/images/slides/slide-3-full.jpg',
                    image_mobile: 'assets/images/slides/slide-3-mobile.jpg',
                    button: this.translateService.instant('primaryBanner.fourth.button'),
                    url: this.translateService.instant('primaryBanner.fourth.url'),
                },
                {
                    title: this.translateService.instant('primaryBanner.fifth.title'),
                    text: this.translateService.instant('primaryBanner.fifth.text'),
                    image_classic: 'assets/images/slides/store/9.png',
                    image_full: 'assets/images/slides/slide-3-full.jpg',
                    image_mobile: 'assets/images/slides/slide-3-mobile.jpg',
                    button: this.translateService.instant('primaryBanner.fifth.button'),
                    url: this.translateService.instant('primaryBanner.fifth.url'),
                }
            ];
        });


    }
}
