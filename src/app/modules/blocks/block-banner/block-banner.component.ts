import { Component } from '@angular/core';
import {TranslateService} from "@ngx-translate/core";

@Component({
    selector: 'app-block-banner',
    templateUrl: './block-banner.component.html',
    styleUrls: ['./block-banner.component.scss']
})
export class BlockBannerComponent {
    constructor(
        translateService: TranslateService,
    ) { }
}
