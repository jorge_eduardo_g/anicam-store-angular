import { Component } from '@angular/core';
import {RootService} from "../../../../shared/services/root.service";
import {TranslateService} from "@ngx-translate/core";

@Component({
    selector: 'app-login',
    templateUrl: './page-login.component.html',
    styleUrls: ['./page-login.component.scss']
})
export class PageLoginComponent {
    constructor(
        public root: RootService,
        public translateService: TranslateService,
    ) { }
}
