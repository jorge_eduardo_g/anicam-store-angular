import { Component, Input } from '@angular/core';
import {Product, ProductFeaturesSection} from '../../../../shared/interfaces/product';
import { specification } from '../../../../../data/shop-product-spec';
import { reviews } from '../../../../../data/shop-product-reviews';
import { Review } from '../../../../shared/interfaces/review';
import {TranslateService} from "@ngx-translate/core";
import {LocalService} from "../../../../shared/services/local.service";

@Component({
    selector: 'app-product-tabs',
    templateUrl: './product-tabs.component.html',
    styleUrls: ['./product-tabs.component.scss']
})
export class ProductTabsComponent {
    @Input() withSidebar = false;
    @Input() tab: 'description'|'specification'|'features'|'reviews' = 'description';
    @Input() product: Product;
    // @Input() specification: ProductFeaturesSection[] = specification;
    reviews: Review[] = reviews;
    language: string;
    constructor(
        public translateService: TranslateService,
        private localService: LocalService
    ) {
        this.language = this.localService.getJsonValue('language')
    }
}
