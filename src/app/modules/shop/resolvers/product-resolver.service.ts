import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Product } from '../../../shared/interfaces/product';
import { EMPTY, Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { RootService } from '../../../shared/services/root.service';
import { ShopService } from '../../../shared/api/shop.service';
import {parseProductsListParams} from "./products-list-resolver.service";

@Injectable({
    providedIn: 'root'
})
export class ProductResolverService implements Resolve<Product> {
    constructor(
        private root: RootService,
        private router: Router,
        private shop: ShopService,
    ) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Product> {
        const productId = route.params.productId || route.data.productId;
        console.log(route.params);
        console.log(route.data);

        return this.shop.getProduct(productId).pipe(
            catchError(error => {

                if (error instanceof HttpErrorResponse && error.status === 404) {
                    return this.shop.getProduct(productId).pipe(
                        catchError(error2 => {
                            if (error2 instanceof HttpErrorResponse && error2.status === 404) {

                                return this.shop.getProduct(productId).pipe(
                                    catchError(error3 => {
                                        if (error3 instanceof HttpErrorResponse && error3.status === 404) {
                                            this.router.navigate([this.root.home()]).then();
                                        }
                                        return EMPTY;
                                    })
                                );

                            }
                            return EMPTY;
                        })
                    );
                }

                return EMPTY;
            })
        );
    }
}
