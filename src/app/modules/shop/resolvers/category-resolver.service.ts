import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { EMPTY, Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { RootService } from '../../../shared/services/root.service';
import { ShopService } from '../../../shared/api/shop.service';

@Injectable({
    providedIn: 'root'
})
export class CategoryResolverService implements Resolve<any> {
    constructor(
        private root: RootService,
        private router: Router,
        private shop: ShopService,
    ) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        let categorySlug = route.params.categorySlug || route.data.categorySlug || null;
        const categoryTitan = route.params.categoryTitan || route.data.categoryTitan || null;
        console.log('eddddd', route.params);

        if(categoryTitan){
            let category = JSON.parse(sessionStorage.getItem('categories'));
            let categoryFound: any;
            for (const item of category) {
                    for (const item1 of item.children) {
                            for (const item2 of item1.children) {
                                if (item2.id === parseInt(categoryTitan)) {
                                    categoryFound = item2;
                                    break;
                                }
                            }
                        if (item1.id === parseInt(categoryTitan)) {
                            categoryFound = item1;
                            break;
                        }
                    }
                if (item.id === parseInt(categoryTitan)) {
                    categoryFound = item;
                    break;
                }
            }
            return categoryFound;
        }

        if(!categorySlug) {
            return null;
        }

            return this.shop.getCategory(categorySlug).pipe(
                catchError(error => {
                    if (error instanceof HttpErrorResponse && error.status === 404) {
                        alert('oops');
                        this.router.navigateByUrl(this.root.notFound()).then();
                    }

                    return EMPTY;
                })
            );

    }
}
