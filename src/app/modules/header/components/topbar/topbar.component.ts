import { Component, OnInit } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import { CurrencyService } from '../../../../shared/services/currency.service';
import {LanguageService} from '../../../../shared/services/language.service';

@Component({
    selector: 'app-header-topbar',
    templateUrl: './topbar.component.html',
    styleUrls: ['./topbar.component.scss']
})
export class TopbarComponent {
    languages = [
        {name: 'header.englishLabel', image: 'usa', code: 'en'},
        {name: 'header.spanishLabel', image: 'colombia', code: 'es'},
    ];

    currencies = [
        {name: 'USD', url: '', code: 'USD', symbol: '$', digitsInfo: '1.2-2',
            locale: 'en-US'},
        {name: 'COP', url: '', code: 'COP', symbol: '$ ', digitsInfo: '1.0-0',
            locale: 'en-US'},
    ];

    constructor(
        public currencyService: CurrencyService,
        public languageService: LanguageService,
        public translateService: TranslateService,
    ) {}

    setCurrency(currency): void {
        this.currencyService.options = {
            code: currency.code,
            display: currency.symbol,
            digitsInfo: currency.digitsInfo,
            locale: currency.locale,
        };
    }

    setLanguage(language) {
        this.languageService.setLanguage(language);
    }
}
