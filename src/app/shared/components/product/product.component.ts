import { Component, Inject, Input, PLATFORM_ID } from '@angular/core';
import { Product } from '../../interfaces/product';
import { FormControl } from '@angular/forms';
import { CartService } from '../../services/cart.service';
import { WishlistService } from '../../services/wishlist.service';
import { CompareService } from '../../services/compare.service';
import { RootService } from '../../services/root.service';
import {TranslateService} from "@ngx-translate/core";
import {Router} from "@angular/router";
import {ShopService} from "../../api/shop.service";

export type ProductLayout = 'standard' | 'sidebar' | 'columnar' | 'quickview';

@Component({
    selector: 'app-product',
    templateUrl: './product.component.html',
    styleUrls: ['./product.component.scss']
})
export class ProductComponent  {
    @Input() layout: ProductLayout;

    @Input() product: Product;

    quantity: FormControl = new FormControl(1);

    addingToCart = false;
    addingToWishlist = false;
    addingToCompare = false;
    variationColor: any;
    variationSize: any;
    quota: any;
    shipping: number;

    constructor(
        @Inject(PLATFORM_ID) private platformId: any,
        private cart: CartService,
        private wishlist: WishlistService,
        private compare: CompareService,
        public root: RootService,
        private router: Router,
        public translateService: TranslateService,
    ) {

    }

    ngOnInit(): void {
        this.variationColor = this.product.variationColor;
        this.variationSize = this.product.variationSize;
    }

    toProduct(id): void {
        this.router.navigate([`/shop/product/${id}`]).then();
    }

    addToCart(): void {
        if (!this.addingToCart && this.product && this.quantity.value > 0 && this.quantity.value <= 4) {
            this.addingToCart = true;
            console.log(this.product);
            this.cart.add(this.product, this.quantity.value).subscribe({complete: () => this.addingToCart = false});

        }
    }

    addToWishlist(): void {
        if (!this.addingToWishlist && this.product) {
            this.addingToWishlist = true;

            this.wishlist.add(this.product).subscribe({complete: () => this.addingToWishlist = false});
        }
    }

    addToCompare(): void {
        if (!this.addingToCompare && this.product) {
            this.addingToCompare = true;

            this.compare.add(this.product).subscribe({complete: () => this.addingToCompare = false});
        }
    }
}
