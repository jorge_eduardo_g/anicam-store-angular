import { Injectable } from '@angular/core';
import * as CryptoJS from 'crypto-js';
import * as SecureStorage from 'secure-web-storage';

const secretKey: any = 'anicam2021';

@Injectable({
    providedIn: 'root'
})
export class StorageService {

    constructor() {}

    public secureStorage = new SecureStorage(localStorage, {

        hash: function hash(key) {
            key = CryptoJS.SHA256(key, secretKey);
            return key.toString();
        },
        encrypt: function encrypt(data) {
            data = CryptoJS.AES.encrypt(data, secretKey);
            data = data.toString();
            return data;
        },
        decrypt: function decrypt(data) {
            data = CryptoJS.AES.decrypt(data, secretKey);
            data = data.toString(CryptoJS.enc.Utf8);
            return data;
        }
    });
}
