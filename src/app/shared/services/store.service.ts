import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class StoreService {
    address = '1770 NW 96 Ave Doral - Florida 33172';
    email = 'operations@anicamenterprises.com';
    phone = ['(786) 245-0709'];
    hours = 'Mon-Sat 08:00am - 5:00pm';

    get primaryPhone(): string|null {
        return this.phone.length > 0 ? this.phone[0] : null;
    }

    constructor() { }
}
