import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { Product } from '../interfaces/product';
import { CartItem } from '../interfaces/cart-item';
import { BehaviorSubject, Observable, Subject, timer } from 'rxjs';
import { map } from 'rxjs/operators';
import { isPlatformBrowser } from '@angular/common';
import {ShopService} from "../api/shop.service";
import {TranslateService} from "@ngx-translate/core";
import {LocalService} from "./local.service";
import {NavigationLink} from "../interfaces/navigation-link";
import {environment} from "../../../environments/environment";
import {HttpClient} from "@angular/common/http";

interface CartTotal {
    title: string;
    price: number;
    type: 'shipping'|'fee'|'tax'|'other';
    inExtend: boolean;
}

interface CartData {
    items: CartItem[];
    quantity: number;
    subtotal: number;
    totals: CartTotal[];
    total: number;
}

@Injectable({
    providedIn: 'root'
})
export class CartService {
    private data: CartData = {
        items: [],
        quantity: 0,
        subtotal: 0,
        totals: [],
        total: 0
    };

    private itemsSubject$: BehaviorSubject<CartItem[]> = new BehaviorSubject(this.data.items);
    private quantitySubject$: BehaviorSubject<number> = new BehaviorSubject(this.data.quantity);
    private subtotalSubject$: BehaviorSubject<number> = new BehaviorSubject(this.data.subtotal);
    private totalsSubject$: BehaviorSubject<CartTotal[]> = new BehaviorSubject(this.data.totals);
    private totalSubject$: BehaviorSubject<number> = new BehaviorSubject(this.data.total);
    private onAddingSubject$: Subject<Product> = new Subject();

    get items(): ReadonlyArray<CartItem> {
        return this.data.items;
    }

    get quantity(): number {
        return this.data.quantity;
    }

    readonly items$: Observable<CartItem[]> = this.itemsSubject$.asObservable();
    readonly quantity$: Observable<number> = this.quantitySubject$.asObservable();
    readonly subtotal$: Observable<number> = this.subtotalSubject$.asObservable();
    readonly totals$: Observable<CartTotal[]> = this.totalsSubject$.asObservable();
    readonly total$: Observable<number> = this.totalSubject$.asObservable();

    readonly onAdding$: Observable<Product> = this.onAddingSubject$.asObservable();

    constructor(
        @Inject(PLATFORM_ID)
        private platformId: any,
        private shop: ShopService,
        public translateService: TranslateService,
        private localService: LocalService,
        private http: HttpClient,
    ) {
        this.translateService.getTranslation(this.localService.getJsonValue('language'));
        if (isPlatformBrowser(this.platformId)) {
            this.load();
            this.calc();
        }
    }

    add(product: Product, quantity: number, options: {name: string; value: string}[] = []): Observable<CartItem> {
        // timer only for demo
        return timer(1500).pipe(map(() => {
            this.onAddingSubject$.next(product);

            let item = this.items.find(eachItem => {
                if (eachItem.product.id !== product.id || eachItem.options.length !== options.length) {
                    return false;
                }

                if (eachItem.options.length) {
                    for (const option of options) {
                        if (!eachItem.options.find(itemOption => itemOption.name === option.name && itemOption.value === option.value)) {
                            return false;
                        }
                    }
                }

                return true;
            });

            if (item) {
//                 this.quota(product).subscribe((r)=>console.log(r))
                item.quantity += quantity;
                this.save();
                this.calc();
            } else {
                this.quota(product).subscribe((quota)=>{
                    console.log(quota);
                    item = {product, quantity, options, quota};
                    this.data.items.push(item);
                    this.save();
                    this.calc();
                    console.log('end');
                })
            }

            return item;
        }));
    }

    update(updates: {item: CartItem, quantity: number}[]): Observable<void> {
        // timer only for demo
        return timer(1000).pipe(map(() => {
            updates.forEach(update => {
                const item = this.items.find(eachItem => eachItem === update.item);

                if (item) {
                    item.quantity = update.quantity;
                }
            });

            this.save();
            this.calc();
        }));
    }

    remove(item: CartItem): Observable<void> {
        // timer only for demo
        return timer(1000).pipe(map(() => {
            this.data.items = this.data.items.filter(eachItem => eachItem !== item);

            this.save();
            this.calc();
        }));
    }

    /**
     * Returns all data country.
     *
     */
    getLocation(): Observable<any> {
        return this.http.get<any>(`${environment.locationService}`);
    }

    private calc(): void {
        let quantity = 0;
        let subtotal = 0;
        let shippingPrice = 0;
        let logisticUs = 0;

            this.data.items.forEach(item => {
            quantity += item.quantity;
            subtotal += item.product.price * item.quantity;
            logisticUs += item.quota.logistic_us;
            shippingPrice += (item.quota.logistic_us + item.quota.insurance + item.quota.duties_tariff + item.quota.iva_vat) * quantity;
        });

        const totals: CartTotal[] = [];

        this.translateService.get(['']).subscribe(translations => {

            this.getLocation().subscribe(loc => {
                if (!loc?.country) {
                    loc.country = 'Colombia';
                }

                totals.push({
                    title: this.translateService.instant('cart.shipping', {country : loc.country}),
                    price: shippingPrice,
                    type: 'shipping',
                    inExtend: false
                });
                /*totals.push({
                    title: 'Logistica',
                    price: logisticUs,
                    type: 'other',
                    inExtend: true
                });*/

                const total = subtotal + totals.reduce((acc, eachTotal) => acc + eachTotal.price, 0);

                this.data.quantity = quantity;
                this.data.subtotal = subtotal;
                this.data.totals = totals;
                this.data.total = total;

                this.itemsSubject$.next(this.data.items);
                this.quantitySubject$.next(this.data.quantity);
                this.subtotalSubject$.next(this.data.subtotal);
                this.totalsSubject$.next(this.data.totals);
                this.totalSubject$.next(this.data.total);
            });

        })

    }

    private save(): void {
        localStorage.setItem('cartItems', JSON.stringify(this.data.items));
    }

    private load(): void {
        const items = localStorage.getItem('cartItems');

        if (items) {
            this.data.items = JSON.parse(items);
        }
    }

    private quota(product: Product): Observable<any> {
        const params = {
            productType: product.categories.join('|'),
            packageHeight: product.customFields.packageHeight,
            packageLength: product.customFields.packageLength,
            packageWeight: product.customFields.packageWeight,
            packageWidth: product.customFields.packageWidth,
            price: product.price
        }

        const subject = new Subject<string>();
        this.shop.getQuota(params).subscribe(result => {
            console.log(result);
            if (result.d?.status === 200) {

                subject.next(result.d.response);
            }
        });
        return subject.asObservable();
    }
}
