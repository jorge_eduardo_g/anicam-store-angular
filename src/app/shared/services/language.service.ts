import {TranslateService} from '@ngx-translate/core';
import {Injectable} from '@angular/core';
import {LocalService} from './local.service';

@Injectable({
    providedIn: 'root'
})
export class LanguageService {

    selectedLanguage;

    constructor(
        private translate: TranslateService,
        private localService: LocalService,
    ) {
        let language = this.localService.getJsonValue('language');
        if (!language) {
            language = navigator.language.split('-')[0];
        }
        this.setLanguage(language);
    }

    setLanguage(language) {
        this.selectedLanguage = language;
        this.localService.setJsonValue('language', language);
        // this.localService.setJsonValue('any', {data: language}, true); set data encrypted
        // console.log(this.localService.getJsonValue('any', true)); get data encrypted
        this.translate.use(language);
    }
}
