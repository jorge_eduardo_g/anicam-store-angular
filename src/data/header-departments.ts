import { NavigationLink } from '../app/shared/interfaces/navigation-link';

export const departments: NavigationLink[] =
    [
    {label: 'Nuestras Categorias', url: '/shop/products', menu: {
            type: 'megamenu',
            size: 'xl',
            image: 'assets/images/megamenu/megamenu-1.jpg',
            columns: [
                {size: 4, items: [
                        {label: 'Hand Tools', url: '/shop/products/Paint Tools', items: [
                                {label: 'Screwdrivers', url: '/shop/products/Screwdrivers'},
                                {label: 'Handsaws', url: '/shop/products/Handsaws'},
                                {label: 'Knives', url: '/shop/products/Knives'},
                                {label: 'Axes', url: '/shop/products/Axes'},
                                {label: 'Multitools', url: '/shop/products/Multitools'},
                                {label: 'Paint Tools', url: '/shop/products/Paint Tools'}
                            ]},
                        {label: 'Garden Equipment', url: '/shop/products/Garden Equipment', items: [
                                {label: 'Motor Pumps', url: '/shop/products/Motor Pumps'},
                                {label: 'Chainsaws', url: '/shop/products/Chainsaws'},
                                {label: 'Electric Saws', url: '/shop/products/Electric Saws'},
                                {label: 'Brush Cutters', url: '/shop/products/Brush Cutters'}
                            ]}
                    ]},
                {size: 4, items: [
                        {label: 'Machine Tools', url: '/shop/products/Machine Tools', items: [
                                {label: 'Thread Cutting', url: '/shop/products/Thread Cutting'},
                                {label: 'Chip Blowers', url: '/shop/products/Chip Blowers'},
                                {label: 'Sharpening Machines', url: '/shop/products/Sharpening Machines'},
                                {label: 'Pipe Cutters', url: '/shop/products/Pipe Cutters'},
                                {label: 'Slotting machines', url: '/shop/products/Slotting machines'},
                                {label: 'Lathes', url: '/shop/products/Lathes'}
                            ]}
                    ]},
                {size: 4, items: [
                        {label: 'Instruments', url: '/shop/products/Instruments', items: [
                                {label: 'Welding Equipment', url: '/shop/products/Welding Equipment'},
                                {label: 'Power Tools', url: '/shop/products/Power Tools'},
                                {label: 'Hand Tools', url: '/shop/products/Hand Tools'},
                                {label: 'Measuring Tool', url: '/shop/products/Measuring Tool'}
                            ]}
                    ]}
            ]
        }},
    {label: 'Power Tools', url: '/shop/products/Power Tools', menu: {
        type: 'megamenu',
        size: 'xl',
        image: 'assets/images/megamenu/megamenu-1.jpg',
        columns: [
            {size: 3, items: [
                {label: 'Power Tools', url: '/shop/products/Power Tools', items: [
                    {label: 'Engravers', url: '/shop/products/Engravers'},
                    {label: 'Drills', url: '/shop/products/Drills'},
                    {label: 'Wrenches', url: '/shop/products/Wrenches'},
                    {label: 'Plumbing', url: '/shop/products/Plumbing'},
                    {label: 'Wall Chaser', url: '/shop/products'},
                    {label: 'Pneumatic Tools', url: '/shop/products'},
                    {label: 'Milling Cutters', url: '/shop/products'}
                ]},
                {label: 'Workbenches', url: '/shop/products'},
                {label: 'Presses', url: '/shop/products'},
                {label: 'Spray Guns', url: '/shop/products'},
                {label: 'Riveters', url: '/shop/products'}
            ]},
            {size: 3, items: [
                {label: 'Hand Tools', url: '/shop/products', items: [
                    {label: 'Screwdrivers', url: '/shop/products'},
                    {label: 'Handsaws', url: '/shop/products'},
                    {label: 'Knives', url: '/shop/products'},
                    {label: 'Axes', url: '/shop/products'},
                    {label: 'Multitools', url: '/shop/products'},
                    {label: 'Paint Tools', url: '/shop/products'}
                ]},
                {label: 'Garden Equipment', url: '/shop/products', items: [
                    {label: 'Motor Pumps', url: '/shop/products'},
                    {label: 'Chainsaws', url: '/shop/products'},
                    {label: 'Electric Saws', url: '/shop/products'},
                    {label: 'Brush Cutters', url: '/shop/products'}
                ]}
            ]},
            {size: 3, items: [
                {label: 'Machine Tools', url: '/shop/products', items: [
                    {label: 'Thread Cutting', url: '/shop/products'},
                    {label: 'Chip Blowers', url: '/shop/products'},
                    {label: 'Sharpening Machines', url: '/shop/products'},
                    {label: 'Pipe Cutters', url: '/shop/products'},
                    {label: 'Slotting machines', url: '/shop/products'},
                    {label: 'Lathes', url: '/shop/products'}
                ]}
            ]},
            {size: 3, items: [
                {label: 'Instruments', url: '/shop/products', items: [
                    {label: 'Welding Equipment', url: '/shop/products'},
                    {label: 'Power Tools', url: '/shop/products'},
                    {label: 'Hand Tools', url: '/shop/products'},
                    {label: 'Measuring Tool', url: '/shop/products'}
                ]}
            ]}
        ]
    }},
    {label: 'Hand Tools', url: '/shop/products', menu: {
        type: 'megamenu',
        size: 'lg',
        image: 'assets/images/megamenu/megamenu-2.jpg',
        columns: [
            {size: 4, items: [
                {label: 'Hand Tools', url: '/shop/products', items: [
                    {label: 'Screwdrivers', url: '/shop/products'},
                    {label: 'Handsaws', url: '/shop/products'},
                    {label: 'Knives', url: '/shop/products'},
                    {label: 'Axes', url: '/shop/products'},
                    {label: 'Multitools', url: '/shop/products'},
                    {label: 'Paint Tools', url: '/shop/products'}
                ]},
                {label: 'Garden Equipment', url: '/shop/products', items: [
                    {label: 'Motor Pumps', url: '/shop/products'},
                    {label: 'Chainsaws', url: '/shop/products'},
                    {label: 'Electric Saws', url: '/shop/products'},
                    {label: 'Brush Cutters', url: '/shop/products'}
                ]}
            ]},
            {size: 4, items: [
                {label: 'Machine Tools', url: '/shop/products', items: [
                    {label: 'Thread Cutting', url: '/shop/products'},
                    {label: 'Chip Blowers', url: '/shop/products'},
                    {label: 'Sharpening Machines', url: '/shop/products'},
                    {label: 'Pipe Cutters', url: '/shop/products'},
                    {label: 'Slotting machines', url: '/shop/products'},
                    {label: 'Lathes', url: '/shop/products'}
                ]}
            ]},
            {size: 4, items: [
                {label: 'Instruments', url: '/shop/products', items: [
                    {label: 'Welding Equipment', url: '/shop/products'},
                    {label: 'Power Tools', url: '/shop/products'},
                    {label: 'Hand Tools', url: '/shop/products'},
                    {label: 'Measuring Tool', url: '/shop/products'}
                ]}
            ]}
        ]
    }},
    {label: 'Machine Tools', url: '/shop/products', menu: {
        type: 'megamenu',
        size: 'nl',
        image: 'assets/images/megamenu/megamenu-3.jpg',
        columns: [
            {size: 6, items: [
                {label: 'Hand Tools', url: '/shop/products', items: [
                    {label: 'Screwdrivers', url: '/shop/products'},
                    {label: 'Handsaws', url: '/shop/products'},
                    {label: 'Knives', url: '/shop/products'},
                    {label: 'Axes', url: '/shop/products'},
                    {label: 'Multitools', url: '/shop/products'},
                    {label: 'Paint Tools', url: '/shop/products'}
                ]},
                {label: 'Garden Equipment', url: '/shop/products', items: [
                    {label: 'Motor Pumps', url: '/shop/products'},
                    {label: 'Chainsaws', url: '/shop/products'},
                    {label: 'Electric Saws', url: '/shop/products'},
                    {label: 'Brush Cutters', url: '/shop/products'}
                ]}
            ]},
            {size: 6, items: [
                {label: 'Instruments', url: '/shop/products', items: [
                    {label: 'Welding Equipment', url: '/shop/products'},
                    {label: 'Power Tools', url: '/shop/products'},
                    {label: 'Hand Tools', url: '/shop/products'},
                    {label: 'Measuring Tool', url: '/shop/products'}
                ]}
            ]}
        ]
    }},
    {label: 'Building Supplies', url: '/shop/products', menu: {
        type: 'megamenu',
        size: 'sm',
        columns: [
            {size: 12, items: [
                {label: 'Hand Tools', url: '/shop/products', items: [
                    {label: 'Screwdrivers', url: '/shop/products'},
                    {label: 'Handsaws', url: '/shop/products'},
                    {label: 'Knives', url: '/shop/products'},
                    {label: 'Axes', url: '/shop/products'},
                    {label: 'Multitools', url: '/shop/products'},
                    {label: 'Paint Tools', url: '/shop/products'}
                ]},
                {label: 'Garden Equipment', url: '/shop/products', items: [
                    {label: 'Motor Pumps', url: '/shop/products'},
                    {label: 'Chainsaws', url: '/shop/products'},
                    {label: 'Electric Saws', url: '/shop/products'},
                    {label: 'Brush Cutters', url: '/shop/products'}
                ]}
            ]}
        ]
    }},
    {label: 'Electrical', url: '/shop/products', menu: {
        type: 'menu',
        items: [
            {label: 'Soldering Equipment', url: '/shop/products', items: [
                {label: 'Soldering Station', url: '/shop/products'},
                {label: 'Soldering Dryers', url: '/shop/products'},
                {label: 'Gas Soldering Iron', url: '/shop/products'},
                {label: 'Electric Soldering Iron', url: '/shop/products'}
            ]},
            {label: 'Light Bulbs', url: '/shop/products'},
            {label: 'Batteries', url: '/shop/products'},
            {label: 'Light Fixtures', url: '/shop/products'},
            {label: 'Warm Floor', url: '/shop/products'},
            {label: 'Generators', url: '/shop/products'},
            {label: 'UPS', url: '/shop/products'}
        ]
    }},
    {label: 'Power Machinery',        url: '/shop/products'},
    {label: 'Measurement',            url: '/shop/products'},
    {label: 'Plumbing',               url: '/shop/products'},
    {label: 'Storage & Organization', url: '/shop/products'},
    {label: 'Welding & Soldering',    url: '/shop/products'}
];
