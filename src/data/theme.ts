export const theme = {
    name: 'Anicam',
    author: {
        name: 'Jorge G',
        profile_url: ''
    },
    company: {
        name: 'Anicam Enterprises',
        profile_url: 'https://anicamenterprises.com'
    }
};
